// SOAL 2

var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

const baca = (time, books, dataBuku) => {
  if (dataBuku < books.length) {
    readBooksPromise(time, books[dataBuku])
      .then((fullfilled) => {
        dataBuku++;
        baca(fullfilled, books, dataBuku);
      })
      .catch((reject) => {
        console.log(reject);
      });
  }
};

baca(10000, books, 0);
