// SOAL 1

function next_date(tanggal, bulan, tahun) {
  var format = { year: "numeric", month: "long", day: "numeric" };
  const tglBaru = new Date(tahun, bulan - 1, tanggal + 1);
  console.log(tglBaru.toLocaleDateString("id", format));
}
next_date(8, 9, 2021);

// SOAL 2
function jumlah_kata(str) {
  var count = str.length;
  return count;
}
var kalimat_1 = jumlah_kata("Halo nama saya Muhammad Iqbal Mubarok");
var kalimat_2 = jumlah_kata("Saya Iqbal");
var kalimat_3 = jumlah_kata("Saya Muhammad Iqbal Mubarok");
console.log(kalimat_1, kalimat_2, kalimat_3);
