// SOAL 1
var nilai;
nilai = 75;

if (nilai >= 85) {
  console.log("A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("D");
} else {
  console.log("E");
}

// SOAL 2
var tanggal = 24;
var bulan = 9;
var tahun = 2001;

switch (bulan) {
  case 1: {
    console.log(24 + " Januari" + " 2001");
    break;
  }
  case 2: {
    console.log(24 + " Februari" + " 2001");
    break;
  }
  case 3: {
    console.log(24 + " Maret" + " 2001");
    break;
  }
  case 4: {
    console.log(24 + " April" + " 2001");
    break;
  }
  case 5: {
    console.log(24 + " Mei" + " 2001");
    break;
  }
  case 6: {
    console.log(24 + " Juni" + " 2001");
    break;
  }
  case 7: {
    console.log(24 + " Juli" + " 2001");
    break;
  }
  case 8: {
    console.log(24 + " Agustus" + " 2001");
    break;
  }
  case 9: {
    console.log(24 + " September" + " 2001");
    break;
  }
  case 10: {
    console.log(24 + " Oktober" + " 2001");
    break;
  }
  case 11: {
    console.log(24 + " November" + " 2001");
    break;
  }
  default: {
    console.log(24 + " Desember" + " 2001");
    break;
  }
}

console.log();

// SOAL 3
// OUTPUT N=3
let hasil = "";
for (let i = 0; i < 3; i++) {
  for (let j = 0; j <= i; j++) {
    hasil += "# ";
  }
  hasil += "\n";
}
console.log(hasil);

// OUTPUT N=7
let hasil2 = "";
for (let i = 0; i < 7; i++) {
  for (let j = 0; j <= i; j++) {
    hasil2 += "# ";
  }
  hasil2 += "\n";
}
console.log(hasil2);

// SOAL 4
// OUTPUT M=3
for (let i = 0; i < 3; i++) {
  if (i === 0) {
    console.log(`${i} - I love programming`);
  } else if (i === 1) {
    console.log(`${i} - I love Javascript`);
  } else {
    console.log(`${i} - I love VueJS`);
  }
}

// OUTPUT M=5

console.log();

for (let i = 1; i <= 5; i++) {
  if (i === 1 || i === 4) {
    console.log(`${i} - I love programming`);
  } else if (i === 2 || i === 5) {
    console.log(`${i} - I love Javascript`);
  } else {
    console.log(`${i} - I love VueJS`);
    console.log("===");
  }
}

console.log();
// OUTPUT M=7
for (let i = 1; i <= 7; i++) {
  if (i === 1 || i === 4 || i === 7) {
    console.log(`${i} - I love programming`);
  } else if (i === 2 || i === 5) {
    console.log(`${i} - I love Javascript`);
  } else {
    console.log(`${i} - I love VueJS`);
    console.log("===");
  }
}

// OUTPUT M=10
console.log();
for (let i = 1; i <= 10; i++) {
  if (i === 1 || i === 4 || i === 7 || i === 10) {
    console.log(`${i} - I love programming`);
  } else if (i === 2 || i === 5 || i === 8) {
    console.log(`${i} - I love Javascript`);
  } else {
    console.log(`${i} - I love VueJS`);
    console.log("===");
  }
}
