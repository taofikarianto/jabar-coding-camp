// SOAL 1
const luasPersegi = (panjang, lebar) => {
  return panjang * lebar;
};

const kelilingPersegi = (panjang, lebar) => {
  return 2 * (panjang * lebar);
};

console.log(luasPersegi(2, 4));
console.log(kelilingPersegi(3, 4));

console.log();

// SOAL 2
let firstName = "William";
let lastName = "Imoh";

const fullName = () => {
  return `${firstName} ${lastName}`;
};

console.log(fullName());

console.log();

// SOAL 3
const newObject = {
  namaPertama: "Muhammad",
  namaKedua: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "Playing Football",
};

const { namaPertama, namaKedua, address, hobby } = newObject;

console.log(namaPertama, namaKedua, address, hobby);

console.log();

// SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

console.log();

// SOAL 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before);
